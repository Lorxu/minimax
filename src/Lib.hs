-- Entirely IO-free module that describes the game and playing method
module Lib
    ( emptyGame,
      Game,
      isOver,
      winner,
      advance,
      rules
    ) where

-- The game state
data Game = Leaf Int | Node Game Game
    deriving Eq
instance Show Game where
    show (Leaf x) = show x
    show (Node x y) = "[" ++ show x ++ "] [" ++ show y ++ "]"

-- Helptext
rules = "The object of the game is to get the highest possible score, while I try to get the lowest possible, and we pick branches based on that. Simply press 1 or 2 to select, every turn."
-- Yes, 1 and 2 not 0 and 1, I know

-- Starting position
emptyGame = gameFromList [1,8,3,4,5,6,2,7]

-- Split a list exactly down the middle
splitList list = splitAt ((length list + 1) `div` 2) list

-- Construct a binary tree from a list
gameFromList (x:[]) = Leaf x -- Leaf node
gameFromList list = Node (gameFromList a) (gameFromList b)
    where (a,b) = splitList list

-- Is the game over?
isOver (Leaf _)   = True
isOver (Node _ _) = False

-- Print score or winner
winner (Leaf x) = "Score of " ++ show x
winner (Node _ _) = "error!!!!!!!" -- isOver is false

-- Send the path the user picked to 'play'
advance (Node x y) "1" = play x
advance (Node x y) "2" = play y
advance _ _ = Leaf (-1000000) -- The user probably picked "3" or "that one"

-- Pick the best path
play (Node x y)
    | a < b = x
    | otherwise = y
    where a = maxV x
          b = maxV y
play x = x -- Leaf node, no choice to make

-- Maximum value of the node
maxV (Leaf x) = x
maxV (Node x y) = max (maxV x) (maxV y)