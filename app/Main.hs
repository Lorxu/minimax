module Main where

import Lib -- Lib describes the game, this file plays it
import System.IO (hFlush, stdout)

main :: IO ()
main = do
    putStrLn "Welcome to Minimax!"
    putStrLn rules
    rec' emptyGame

rec' :: Game -> IO ()
rec' game
    | isOver game = do
        print game
        putStrLn $ winner game
    | otherwise   = do
        print game
        putStr "Your move: "
        hFlush stdout
        move <- getLine
        rec' $ advance game move